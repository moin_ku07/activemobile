//
//  HomeNavigationViewController.swift
//  FoodMenu
//
//  Created by Moin Uddin on 7/26/16.
//  Copyright © 2016 Moin Uddin. All rights reserved.
//

import UIKit

class HomeNavigationViewController: UINavigationController, SideBarDelegate {
    
    var sideBar: SideBar!
    var overlayView: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationBar.tintColor = UIColor(hex: "404243")
        self.navigationBar.shadowImage = UIImage()
        self.navigationBar.barTintColor = UIColor.white
        self.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationBar.setBottomBorderColor(UIColor(hex: "cfcfcf"), height: 1.0)
        //UINavigationBar.appearance().titleTextAttributes = [NSFontAttributeName: UIFont(name: "Avenir-Heavy", size: 16)!, NSForegroundColorAttributeName: UIColor.blackColor()]
        
        /*for fontname in UIFont.fontNamesForFamilyName("Avenir"){
            print("   \(fontname)")
        }*/

        overlayView = UIView(frame: self.view.frame)
        overlayView.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        overlayView.isHidden = true
        self.view.addSubview(overlayView)
        
        let overlayTap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(toggleSideBar))
        overlayTap.numberOfTapsRequired = 1
        overlayView.isUserInteractionEnabled = true
        overlayView.addGestureRecognizer(overlayTap)
        
        let menuvc: MenuTableViewController = self.storyboard?.instantiateViewController(withIdentifier: "MenuTableViewController") as! MenuTableViewController
        menuvc.delegate = self.viewControllers.first as? HomeViewController
        
        sideBar = SideBar(sourceView: self.view, menuViewController: menuvc)
        sideBar.delegate = self
    }
    
    func toggleSideBar(){
        if sideBar.isSideBarOpen{
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(250 * Int64(NSEC_PER_MSEC)) / Double(NSEC_PER_SEC)) {
                self.overlayView.isHidden = true
            }
            sideBar.showSideBar(false)
            UIApplication.shared.statusBarStyle = .default
        }else{
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(250 * Int64(NSEC_PER_MSEC)) / Double(NSEC_PER_SEC)) {
                self.overlayView.isHidden = false
            }
            sideBar.showSideBar(true)
            UIApplication.shared.statusBarStyle = .lightContent
        }
    }
    
    func sideBarWillOpen() {
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(250 * Int64(NSEC_PER_MSEC)) / Double(NSEC_PER_SEC)) {
            self.overlayView.isHidden = false
        }
        UIApplication.shared.statusBarStyle = .lightContent
    }
    
    func sideBarWillClose() {
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(250 * Int64(NSEC_PER_MSEC)) / Double(NSEC_PER_SEC)) {
            self.overlayView.isHidden = true
        }
        UIApplication.shared.statusBarStyle = .default
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
