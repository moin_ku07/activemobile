//
//  MapViewController.swift
//  FoodMenu
//
//  Created by Moin Uddin on 7/25/16.
//  Copyright © 2016 Moin Uddin. All rights reserved.
//

import UIKit
import GoogleMaps

class MapViewController: UIViewController, GMSMapViewDelegate, UITextFieldDelegate {
    
    @IBOutlet weak var searchField: UITextField!
    @IBOutlet weak var mapView: GMSMapView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.parent?.navigationItem.title = NSLocalizedString("Label_Restaurant_Finder", comment: "Restaurant Finder").uppercased()
        if UIApplication.shared.userInterfaceLayoutDirection == UIUserInterfaceLayoutDirection.rightToLeft || Locale.preferredLanguages[0].hasPrefix("ar"){
            if UIDevice.current.systemVersion.hasPrefix("8"){
                self.parent?.navigationItem.setLeftBarButton(nil, animated: true)
            }
        }else{
            self.parent?.navigationItem.setRightBarButton(nil, animated: false)
        }
        
        let searchImageView: UIImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 45, height: 18))
        searchImageView.contentMode = .scaleAspectFit
        searchImageView.image = UIImage(named: "icon-search")
        searchField.delegate = self
        
        if UIApplication.shared.userInterfaceLayoutDirection == UIUserInterfaceLayoutDirection.rightToLeft || Locale.preferredLanguages[0].hasPrefix("ar"){
            var scalingTransform : CGAffineTransform!
            scalingTransform = CGAffineTransform(scaleX: -1, y: 1)
            searchImageView.transform = scalingTransform
            if UIDevice.current.systemVersion.hasPrefix("8"){
                searchField.rightViewMode = .always
                searchField.rightView = searchImageView
                searchField.textAlignment = .right
            }else{
                searchField.leftViewMode = .always
                searchField.leftView = searchImageView
            }
        }else{
            searchField.leftViewMode = .always
            searchField.leftView = searchImageView
        }
        
        let borderBottom = CALayer()
        let borderWidth = CGFloat(2.0)
        borderBottom.borderColor = UIColor(hex: "d3d3d3").cgColor
        borderBottom.frame = CGRect(x: 0, y: searchField.frame.height - 1.0, width: searchField.frame.width , height: searchField.frame.height - 1.0)
        borderBottom.borderWidth = borderWidth
        searchField.layer.addSublayer(borderBottom)
        searchField.layer.masksToBounds = true

        let camera = GMSCameraPosition.camera(withLatitude: 24.7494029, longitude: 46.90283750000003, zoom: 10)
        mapView.delegate = self
        mapView.camera = camera
        mapView.isMyLocationEnabled = true
        
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2DMake(24.7494029, 46.90283750000003)
        marker.icon = UIImage(named: "marker-giftshop")
        marker.map = mapView
        
        let marker2 = GMSMarker(position: CLLocationCoordinate2D(latitude: 24.72758540029345, longitude: 46.663966049121086))
        marker2.icon = UIImage(named: "marker-giftshop")
        marker2.map = mapView
        
        let marker3 = GMSMarker(position: CLLocationCoordinate2D(latitude: 24.666449280943358, longitude: 46.857256759570305))
        marker3.icon = UIImage(named: "marker-giftshop")
        marker3.map = mapView
    }
    
    func mapView(_ mapView: GMSMapView, markerInfoWindow marker: GMSMarker) -> UIView? {
        if let infoView: UIView = Bundle.main.loadNibNamed("MapInfoView", owner: self, options: nil)?.first as? MapInfoView{
            return infoView
        }
        
        return nil
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
