//
//  MenuTableViewController.swift
//  FoodMenu
//
//  Created by Moin Uddin on 7/26/16.
//  Copyright © 2016 Moin Uddin. All rights reserved.
//

import UIKit

protocol MenuTableViewControllerDelegate{
    func onMenuTap(_ index: Int, data: [String: String]?)
    func onCloseTap()
    func onLogoutTap()
}

class MenuTableViewController: UITableViewController {
    
    var delegate: MenuTableViewControllerDelegate?
    
    var tableData: [[String: String]] = [
        ["title": NSLocalizedString("Label_MyProfile", comment: "My Profile"), "icon": "icon-profile"],
        ["title": NSLocalizedString("Label_MenuList", comment: "Menu List"), "icon": "icon-menu"],
        ["title": NSLocalizedString("Label_OnlineOrder", comment: "Online Ordering"), "icon": "icon-order"],
        ["title": NSLocalizedString("Label_Feedback", comment: "Feedback"), "icon": "icon-feedback"],
        ["title": NSLocalizedString("Label_Restaurant_Finder", comment: "Restaurant Finder"), "icon": "icon-finder"],
    ]

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return tableData.count
    }

    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44.0
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44.0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: MenuTableViewCell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! MenuTableViewCell

        let cellData: [String: String] = tableData[indexPath.row]
        
        cell.iconView.image = UIImage(named: cellData["icon"]!)
        cell.titleLabel.text = cellData["title"]

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.onMenuTap(indexPath.row, data: tableData[indexPath.row])
        tableView.deselectRow(at: indexPath, animated: true)
    }
 

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    @IBAction func onCloseButtonTap(_ sender: UIButton) {
        delegate?.onCloseTap()
    }
    
    
    @IBAction func onLogoutButtonTap(_ sender: UIButton) {
        delegate?.onLogoutTap()
    }

}
