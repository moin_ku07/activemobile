//
//  FoodMenuTableViewController.swift
//  ActiveMobile
//
//  Created by Moin Uddin on 8/13/16.
//  Copyright © 2016 Moin Uddin. All rights reserved.
//

import UIKit

class FoodMenuTableViewController: UITableViewController {
    
    var pageIndex: Int = 0
    
    var tableData: [[String: String]] = [
        ["title": "Burger", "subtitle": "Side dishes, america", "description": "onion, baby carrots, garlic clove", "price": "SAR 50 for one", "thumb": "thumb1"],
        ["title": "Ice Cream With Sprinkles", "subtitle": "ice cream, London", "description": "food, ice cream, sprinkles, want, yum", "price": "SAR 75 for one", "thumb": "thumb2"],
        ["title": "Apple Pie ", "subtitle": "dish, Western Europe", "description": "apple pie, autumn, cute, food, healthy", "price": "SAR 110 for one", "thumb": "thumb3"],
        ["title": "Fries before guys", "subtitle": "Side dishes, america", "description": "bae, food, french fries, mc donalds", "price": "SAR 110 for one", "thumb": "thumb4"],
        ["title": "Rose Ice Cream", "subtitle": "Side dishes, america", "description": "onion, baby carrots, garlic clove", "price": "SAR 50 for one", "thumb": "thumb5"]
    ]

    override func viewDidLoad() {
        super.viewDidLoad()

        if UIApplication.shared.userInterfaceLayoutDirection == UIUserInterfaceLayoutDirection.rightToLeft || Locale.preferredLanguages[0].hasPrefix("ar"){
            if UIDevice.current.systemVersion.hasPrefix("8"){
                var scalingTransform : CGAffineTransform!
                scalingTransform = CGAffineTransform(scaleX: -1, y: 1);
                tableView.transform = scalingTransform
            }
            
            tableData = [
                ["title": "برغر", "subtitle": "طبق رئيسي، من المطبخ الامريكي", "description": "بصل، جزر، ثوم اخضر، وصلصة المايونيز", "price": "50 ريال", "thumb": "thumb1"],
                ["title": "آيسكريم النجوم", "subtitle": "آيسكريم لندن", "description": "آيسكريم فانيلا، فراولة، مزين بحبيبات الشوكلاته", "price": "35 ريال", "thumb": "thumb2"],
                ["title": "فطيرة التفاح", "subtitle": "طبق اصيل من اوروبا الغربية", "description": "يحضر طازجاً من التفاح الطبيعي و الزبدة", "price": "45 ريال", "thumb": "thumb3"],
                ["title": "بطاطس", "subtitle": "طلب جانبي", "description": "محضر من أجود أنواع البطاطس", "price": "20 ريال", "thumb": "thumb4"],
                ["title": "ايسكريم روز", "subtitle": "حلويات", "description": "قطعة من الكيك مزينة بالآيسكريم روز", "price": "50 ريال", "thumb": "thumb5"]
            ]
        }
        
        //if pageIndex != 0{
            let lastIndex: Int = Int(arc4random_uniform(4 - 1) + 1)
            tableData = Array(tableData.shuffle()[0...lastIndex])
       // }
        
        tableView.reloadData()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return tableData.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: FoodMenuTableViewCell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! FoodMenuTableViewCell
        
        let cellData: [String: String] = tableData[indexPath.row]
        
        cell.thumb.image = UIImage(named: cellData["thumb"]!)
        cell.title.text = cellData["title"]
        cell.subtitle.text = cellData["subtitle"]
        cell.descriptionLabel.text = cellData["description"]
        cell.priceLabel.text = cellData["price"]
        
        return cell
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension Collection {
    /// Return a copy of `self` with its elements shuffled
    func shuffle() -> [Iterator.Element] {
        var list = Array(self)
        list.shuffleInPlace()
        return list
    }
}

extension MutableCollection where Index == Int {
    /// Shuffle the elements of `self` in-place.
    mutating func shuffleInPlace() {
        // empty and single-element collections don't shuffle
        if count < 2 { return }
        
        for i in startIndex..<endIndex {
            let j = Int(arc4random_uniform(UInt32(endIndex - i))) + i
            if i != j {
                swap(&self[i], &self[j])
            }
        }
    }
}
