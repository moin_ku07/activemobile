//
//  FoodMenuViewController.swift
//  FoodMenu
//
//  Created by Moin Uddin on 7/26/16.
//  Copyright © 2016 Moin Uddin. All rights reserved.
//

import UIKit

class FoodMenuViewController: UIViewController{
    
    @IBOutlet weak var filterButton1: UIButton!
    @IBOutlet weak var filterButton2: UIButton!
    @IBOutlet weak var filterButton3: UIButton!
    @IBOutlet weak var filterButton4: UIButton!
    @IBOutlet weak var pageViewContainer: UIView!
    
    var pageViewController: PageViewControllerNoBounce!
    
    var totalPage: Int = 4
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.parent?.navigationItem.title = NSLocalizedString("Label_MenuList", comment: "MENU LIST").uppercased()
        
        let viewOrder: String = NSLocalizedString("Label_ViewOrder", comment: "View Order")
        let rightBarButton: UIBarButtonItem = UIBarButtonItem(title: "\(viewOrder) \u{203A}", style: UIBarButtonItemStyle.plain, target: self, action: nil)
        rightBarButton.setTitleTextAttributes([NSFontAttributeName : UIFont(name: "Avenir-Book", size: 12)!], for: UIControlState())
        
        if UIApplication.shared.userInterfaceLayoutDirection == UIUserInterfaceLayoutDirection.rightToLeft || Locale.preferredLanguages[0].hasPrefix("ar"){
            if UIDevice.current.systemVersion.hasPrefix("8"){
                //self.parentViewController?.navigationItem.leftBarButtonItem = rightBarButton
            }
        }else{
            //self.parentViewController?.navigationItem.rightBarButtonItem = rightBarButton
        }
        
        for button in [filterButton1, filterButton2, filterButton3, filterButton4]{
            button?.addTarget(self, action: #selector(onFilterButtonTap(_:)), for: UIControlEvents.touchUpInside)
        }
        
        pageViewController = PageViewControllerNoBounce(transitionStyle: UIPageViewControllerTransitionStyle.scroll, navigationOrientation: UIPageViewControllerNavigationOrientation.horizontal, options: nil)
        pageViewController.delegate = self
        pageViewController.dataSource = self
        //pageViewController.disableBounce = true
        pageViewController.totalPage = totalPage
        
        pageViewController.view.backgroundColor = UIColor.white
        
        //self.automaticallyAdjustsScrollViewInsets = false
        
        let vc: FoodMenuTableViewController = self.storyboard?.instantiateViewController(withIdentifier: "FoodMenuTableViewController") as! FoodMenuTableViewController
        vc.pageIndex = 0
        
        pageViewController.setViewControllers([vc], direction: UIPageViewControllerNavigationDirection.forward, animated: false, completion: nil)
        
        self.addChildViewController(pageViewController)
        let pageView: UIView = pageViewController.view
        pageView.translatesAutoresizingMaskIntoConstraints = false
        self.pageViewContainer.addSubview(pageViewController.view)
        self.pageViewContainer.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[pageView]-0-|", options: NSLayoutFormatOptions(), metrics: nil, views: ["pageView": pageView]))
        self.pageViewContainer.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-0-[pageView]-0-|", options: NSLayoutFormatOptions(), metrics: nil, views: ["pageView": pageView]))
        pageViewController.didMove(toParentViewController: self)
        
        if UIApplication.shared.userInterfaceLayoutDirection == UIUserInterfaceLayoutDirection.rightToLeft || Locale.preferredLanguages[0].hasPrefix("ar"){
            if UIDevice.current.systemVersion.hasPrefix("8"){
                var scalingTransform : CGAffineTransform!
                scalingTransform = CGAffineTransform(scaleX: -1, y: 1);
                pageViewController.view.transform = scalingTransform
            }
        }
    }
    
    func onFilterButtonTap(_ sender: UIButton){
        selectFilterButton(sender)
        
        var selectedPageIndex: Int = 0
        var direction: UIPageViewControllerNavigationDirection = .forward
        
        if sender == filterButton1{
            selectedPageIndex = 0
        }else if sender == filterButton2{
            selectedPageIndex = 1
        }else if sender == filterButton3{
            selectedPageIndex = 2
        }else if sender == filterButton4{
            selectedPageIndex = 3
        }
        if let vc: FoodMenuTableViewController = pageViewController.viewControllers?.first as? FoodMenuTableViewController{
            //print("prevIndex = \(vc.pageIndex), newIndex = \(selectedPageIndex)")
            let vc1: FoodMenuTableViewController = getItemController(selectedPageIndex) as! FoodMenuTableViewController!
            direction = .forward
            if (UIApplication.shared.userInterfaceLayoutDirection == UIUserInterfaceLayoutDirection.rightToLeft || Locale.preferredLanguages[0].hasPrefix("ar")) && !UIDevice.current.systemVersion.hasPrefix("8"){
                direction = .reverse
            }
            if selectedPageIndex > vc.pageIndex{
                pageViewController.setViewControllers([vc1], direction: direction, animated: true, completion: {(finished: Bool)-> Void in
                    self.pageViewController.currentIndex = vc1.pageIndex
                })
            }else if selectedPageIndex < vc.pageIndex{
                direction = .reverse
                if (UIApplication.shared.userInterfaceLayoutDirection == UIUserInterfaceLayoutDirection.rightToLeft || Locale.preferredLanguages[0].hasPrefix("ar")) && !UIDevice.current.systemVersion.hasPrefix("8"){
                    direction = .forward
                }
                pageViewController.setViewControllers([vc1], direction: direction, animated: true, completion: {(finished: Bool)-> Void in
                    self.pageViewController.currentIndex = vc1.pageIndex
                })
            }
        }else{
            //print("ek")
            let vc1: FoodMenuTableViewController = getItemController(selectedPageIndex) as! FoodMenuTableViewController!
            pageViewController.setViewControllers([vc1], direction: UIPageViewControllerNavigationDirection.forward, animated: false, completion: {(finished: Bool)-> Void in
                self.pageViewController.currentIndex = vc1.pageIndex
            })
        }
    }
    
    func selectFilterButton(_ sender: UIButton){
        for button in [filterButton1, filterButton2, filterButton3, filterButton4]{
            button?.isSelected = false
        }
        sender.isSelected = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

extension FoodMenuViewController: UIPageViewControllerDataSource, UIPageViewControllerDelegate{
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        let currentIndex: Int = (viewController as! FoodMenuTableViewController).pageIndex
        
        if currentIndex > 0{
            return getItemController(currentIndex-1)
        }
        
        return nil
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        let currentIndex: Int = (viewController as! FoodMenuTableViewController).pageIndex
        
        if currentIndex + 1 < totalPage {
            return getItemController(currentIndex + 1)
        }
        
        return nil
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if let vc: FoodMenuTableViewController = pageViewController.viewControllers?.first as? FoodMenuTableViewController{
            self.pageViewController.currentIndex = vc.pageIndex
            //print("currentIndex = \(vc.pageIndex)")
            if vc.pageIndex == 0{
                selectFilterButton(filterButton1)
            }else if vc.pageIndex == 1{
                selectFilterButton(filterButton2)
            }else if vc.pageIndex == 2{
                selectFilterButton(filterButton3)
            }else if vc.pageIndex == 3{
                selectFilterButton(filterButton4)
            }
        }
    }
    
    // MARK: - getItemController
    func getItemController(_ index: Int) -> UIViewController?{
        
        if index < totalPage{
            let vc: FoodMenuTableViewController = self.storyboard?.instantiateViewController(withIdentifier: "FoodMenuTableViewController") as! FoodMenuTableViewController
            vc.pageIndex = index
            return vc
        }
        
        return nil
    }
}
