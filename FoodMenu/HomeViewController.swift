//
//  HomeViewController.swift
//  FoodMenu
//
//  Created by Moin Uddin on 7/26/16.
//  Copyright © 2016 Moin Uddin. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController, MenuTableViewControllerDelegate {

    @IBOutlet weak var menuButton: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if UIApplication.shared.userInterfaceLayoutDirection == UIUserInterfaceLayoutDirection.rightToLeft || Locale.preferredLanguages[0].hasPrefix("ar"){
            if UIDevice.current.systemVersion.hasPrefix("8"){
                let rightBarButton: UIBarButtonItem? = self.navigationItem.rightBarButtonItem
                self.navigationItem.rightBarButtonItem = self.navigationItem.leftBarButtonItem
                self.navigationItem.leftBarButtonItem = rightBarButton
            }
        }

        //let vc: FoodMenuViewController = self.storyboard?.instantiateViewControllerWithIdentifier("FoodMenuViewController") as! FoodMenuViewController
        let vc: LoginViewController = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        self.addChildViewController(vc)
        vc.view.frame = self.view.frame
        self.view.addSubview(vc.view)
        vc.didMove(toParentViewController: self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onMenuTap(_ sender: UIBarButtonItem) {
        (self.navigationController as! HomeNavigationViewController).toggleSideBar()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func onLogoutTap() {
        //(UIApplication.sharedApplication().delegate as! AppDelegate).window?.rootViewController = self.storyboard?.instantiateViewControllerWithIdentifier("LoginViewController") as? LoginViewController
        
        let vc: LoginViewController = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        self.addChildViewController(vc)
        vc.view.frame = self.view.frame
        self.view.addSubview(vc.view)
        vc.didMove(toParentViewController: self)
        onCloseTap()
        
    }
    
    func onCloseTap() {
        (self.navigationController as! HomeNavigationViewController).toggleSideBar()
    }
    
    func onMenuTap(_ index: Int, data: [String : String]?) {
        if index == 1{
            setMenuViewAsActive()
            onCloseTap()
        }else if index == 4{
            setFinderAsActive()
            onCloseTap()
        }
    }
    
    func setMenuViewAsActive(){
        let vc: FoodMenuViewController = self.storyboard?.instantiateViewController(withIdentifier: "FoodMenuViewController") as! FoodMenuViewController
        self.addChildViewController(vc)
        vc.view.frame = self.view.frame
        self.view.removeAllSubviews()
        self.view.addSubview(vc.view)
        vc.didMove(toParentViewController: self)
    }
    
    func setFinderAsActive(){
        let vc: MapViewController = self.storyboard?.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
        self.addChildViewController(vc)
        vc.view.frame = self.view.frame
        self.view.removeAllSubviews()
        self.view.addSubview(vc.view)
        vc.didMove(toParentViewController: self)
    }

}

extension UIView{
    func removeAllSubviews(){
        for subview in subviews{
            subview.removeFromSuperview()
        }
    }
}
