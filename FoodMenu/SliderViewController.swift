//
//  SliderViewController.swift
//  FoodMenu
//
//  Created by Moin Uddin on 7/25/16.
//  Copyright © 2016 Moin Uddin. All rights reserved.
//

import UIKit

class SliderViewController: UIViewController, UIPageViewControllerDelegate, UIPageViewControllerDataSource {
    
    var pageViewController: UIPageViewController!
    var pageControl: UIPageControl!
    
    var totalPage: Int = 3

    override func viewDidLoad() {
        super.viewDidLoad()

        pageViewController = UIPageViewController(transitionStyle: UIPageViewControllerTransitionStyle.scroll, navigationOrientation: UIPageViewControllerNavigationOrientation.horizontal, options: nil)
        pageViewController.delegate = self
        pageViewController.dataSource = self
        
        let vc: ItemViewController = self.storyboard?.instantiateViewController(withIdentifier: "ItemViewController") as! ItemViewController
        vc.pageIndex = 0
        pageViewController.setViewControllers([vc], direction: UIPageViewControllerNavigationDirection.forward, animated: false, completion: nil)
        
        
        self.addChildViewController(pageViewController)
        self.view.addSubview(pageViewController.view)
        pageViewController.didMove(toParentViewController: self)
        
        pageControl = UIPageControl(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 20))
        pageControl.numberOfPages = totalPage
        pageControl.backgroundColor = UIColor.clear
        pageControl.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(pageControl)
        
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[pageControl]-0-|", options: NSLayoutFormatOptions(), metrics: nil, views: ["pageControl": pageControl]))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[pageControl(20)]-0-|", options: NSLayoutFormatOptions(), metrics: nil, views: ["pageControl": pageControl]))
        
        let appearance = UIPageControl.appearance()
        appearance.pageIndicatorTintColor = UIColor(patternImage: UIImage(named: "dot")!)
        appearance.currentPageIndicatorTintColor = UIColor.white
        appearance.backgroundColor = UIColor.clear
        
        if UIApplication.shared.userInterfaceLayoutDirection == UIUserInterfaceLayoutDirection.rightToLeft || Locale.preferredLanguages[0].hasPrefix("ar"){
            if UIDevice.current.systemVersion.hasPrefix("8"){
                var scalingTransform : CGAffineTransform!
                scalingTransform = CGAffineTransform(scaleX: -1, y: 1);
                pageViewController.view.transform = scalingTransform
                
                pageControl.transform = scalingTransform
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // MARK: - UIPageViewControllerDataSource
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        let vc = viewController as! ItemViewController
        
        if vc.pageIndex > 0 {
            return getItemController(vc.pageIndex-1)
        }
        
        return nil
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        let vc = viewController as! ItemViewController
        
        if vc.pageIndex+1 < totalPage {
            return getItemController(vc.pageIndex+1)
        }
        
        return nil
    }
    
    // MARK: - getItemController
    func getItemController(_ index: Int) -> UIViewController?{
        
        if index < totalPage{
            let vc: ItemViewController = self.storyboard?.instantiateViewController(withIdentifier: "ItemViewController") as! ItemViewController
            vc.pageIndex = index
            return vc
        }
        
        return nil
    }
    
    
    // MARK: - Page Indicator
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        self.pageControl.currentPage = (pageViewController.viewControllers!.first as! ItemViewController).pageIndex
    }

}
