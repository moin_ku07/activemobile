//
//  PageItemViewController.swift
//  PEEMZ
//
//  Created by Moin Uddin on 7/28/16.
//  Copyright © 2016 David Ghouzi. All rights reserved.
//

import UIKit

class PageViewControllerNoBounce: UIPageViewController, UIScrollViewDelegate{
    
    var currentIndex: Int = 0
    var totalPage: Int = 0
    
    var pageScrollView: UIScrollView!
    
    var disableBounce: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        for view in self.view.subviews {
            if let scrollView = view as? UIScrollView {
                scrollView.delegate = self
                pageScrollView = scrollView
            }
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        //print("currentIndex = \(currentIndex)")
        //print("totalPage - 1 = \(totalPage - 1)")
        if disableBounce && navigationOrientation == .vertical{
            if currentIndex == 0 && scrollView.contentOffset.y < scrollView.bounds.size.height{
                scrollView.contentOffset = CGPoint(x: 0, y: scrollView.bounds.size.height)
            }else if currentIndex == totalPage - 1 && scrollView.contentOffset.y > scrollView.bounds.size.height{
                scrollView.contentOffset = CGPoint(x: 0, y: scrollView.bounds.size.height)
            }
        }else if disableBounce && navigationOrientation == .horizontal{
            if currentIndex == 0 && scrollView.contentOffset.x < scrollView.bounds.size.width{
                scrollView.contentOffset = CGPoint(x: scrollView.bounds.size.width, y: 0)
            }else if currentIndex == totalPage - 1 && scrollView.contentOffset.x > scrollView.bounds.size.width{
                scrollView.contentOffset = CGPoint(x: scrollView.bounds.size.width, y: 0)
            }
        }
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        if disableBounce && navigationOrientation == .vertical{
            if currentIndex == 0 && scrollView.contentOffset.y < scrollView.bounds.size.height{
                scrollView.contentOffset = CGPoint(x: 0, y: scrollView.bounds.size.height)
            }else if currentIndex == totalPage - 1 && scrollView.contentOffset.y > scrollView.bounds.size.height{
                scrollView.contentOffset = CGPoint(x: 0, y: scrollView.bounds.size.height)
            }
        }else if disableBounce && navigationOrientation == .horizontal{
            if currentIndex == 0 && scrollView.contentOffset.x < scrollView.bounds.size.width{
                scrollView.contentOffset = CGPoint(x: scrollView.bounds.size.width, y: 0)
            }else if currentIndex == totalPage - 1 && scrollView.contentOffset.x > scrollView.bounds.size.width{
                scrollView.contentOffset = CGPoint(x: scrollView.bounds.size.width, y: 0)
            }
        }
    }
}

class PageItemViewController: UIViewController {
    
    var pageIndex: Int = 0

}

class PageItemTableViewController: UITableViewController {
    
    var pageIndex: Int = 0
    
}

class PageItemNavigationViewController: UINavigationController {
    
    var pageIndex: Int = 0
    
}

class PageItemCollectionViewController: UICollectionViewController{
    
    var pageIndex: Int = 0
    
}
