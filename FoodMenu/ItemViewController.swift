//
//  ItemViewController.swift
//  FoodMenu
//
//  Created by Moin Uddin on 7/25/16.
//  Copyright © 2016 Moin Uddin. All rights reserved.
//

import UIKit

class ItemViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    var pageIndex: Int = 0
    
    let images: [String] = ["slider1", "slider5","slider3"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if UIApplication.shared.userInterfaceLayoutDirection == UIUserInterfaceLayoutDirection.rightToLeft || Locale.preferredLanguages[0].hasPrefix("ar"){
            if UIDevice.current.systemVersion.hasPrefix("8"){
                var scalingTransform : CGAffineTransform!
                scalingTransform = CGAffineTransform(scaleX: -1, y: 1);
                view.transform = scalingTransform
            }
        }
        
        imageView.image = UIImage(named: images[pageIndex])
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
