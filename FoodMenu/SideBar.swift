//
//  SideBar.swift
//  BlurrySideBar
//
//  Created by Moin Uddin on 11/13/14.
//  Copyright (c) 2014 Moin Uddin. All rights reserved.
//

import UIKit

@objc protocol SideBarDelegate{
    @objc optional func sideBarDidSelectRowAtIndex(_ index: Int)
    @objc optional func sideBarWillClose()
    @objc optional func sideBarWillOpen()
}

class SideBar: NSObject/*, SideBarTableViewControllerDelegate */{
    
    let barWidth: CGFloat = 250.0
    let sideBarTableViewTopInset: CGFloat = 64.0
    let sideBarContainerView: UIView = UIView()
    var sideBarTableViewController: UITableViewController!
    var originView: UIView!
    var animator: UIDynamicAnimator!
    var delegate: SideBarDelegate?
    var isSideBarOpen: Bool = false
    var shouldCloseOnSelection = false
    var shouldDeselectSelectedRow = false
    var shouldHandleSwipe: Bool = true
    
    var tag: Int?
    
    override init() {
        super.init()
    }
    
    init(sourceView: UIView, menuViewController: UITableViewController) {
        super.init()
        
        originView = sourceView
        //sideBarTableViewController.tableData = menuItems
        sideBarTableViewController = menuViewController
        
        setupSideBar()
        
        animator = UIDynamicAnimator(referenceView: originView)
        
        let showGestureRecognizer: UISwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(SideBar.handleSwipe(_:)))
        showGestureRecognizer.direction = .right
        if !(UIApplication.shared.userInterfaceLayoutDirection == UIUserInterfaceLayoutDirection.rightToLeft && UIDevice.current.systemVersion.hasPrefix("8")){
            originView.addGestureRecognizer(showGestureRecognizer)
        }
        
        let hideGestureRecognizer: UISwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(SideBar.handleSwipe(_:)))
        hideGestureRecognizer.direction = UISwipeGestureRecognizerDirection.left
        if !(UIApplication.shared.userInterfaceLayoutDirection == UIUserInterfaceLayoutDirection.rightToLeft && UIDevice.current.systemVersion.hasPrefix("8")){
            originView.addGestureRecognizer(hideGestureRecognizer)
        }
    }
    
    func setupSideBar(){
        sideBarContainerView.frame = CGRect(x: -barWidth-1, y: originView.frame.origin.y, width: barWidth, height: originView.frame.size.height)
        //print(sideBarContainerView.frame)
        if UIApplication.shared.userInterfaceLayoutDirection == UIUserInterfaceLayoutDirection.rightToLeft || Locale.preferredLanguages[0].hasPrefix("ar"){
            sideBarContainerView.frame = CGRect(x: originView.frame.size.width + 1, y: originView.frame.origin.y, width: barWidth, height: originView.frame.size.height)
        }
        //print(sideBarContainerView.frame)
        sideBarContainerView.backgroundColor = UIColor.red
        sideBarContainerView.clipsToBounds = false
        
        originView.addSubview(sideBarContainerView)
        
        /*if self.isIOS8(){
            let blurView: UIVisualEffectView = UIVisualEffectView(effect: UIBlurEffect(style: UIBlurEffectStyle.Light))
            blurView.frame = sideBarContainerView.bounds
            sideBarContainerView.addSubview(blurView)
        }else{*/
            sideBarContainerView.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.9)
        //}
        
        //sideBarTableViewController.delegate = self
        sideBarTableViewController.tableView.frame = sideBarContainerView.bounds
        sideBarTableViewController.tableView.clipsToBounds = false
        /*sideBarTableViewController.tableView.separatorStyle = UITableViewCellSeparatorStyle.None
        sideBarTableViewController.tableView.backgroundColor = UIColor.clearColor()
        sideBarTableViewController.tableView.scrollsToTop = false
        sideBarTableViewController.tableView.contentInset = UIEdgeInsetsMake(sideBarTableViewTopInset, 0, 0, 0)
        
        sideBarTableViewController.tableView.reloadData()*/
        
        sideBarContainerView.addSubview(sideBarTableViewController.tableView)
    }
    
    func handleSwipe(_ recognizer: UISwipeGestureRecognizer){
        if self.shouldHandleSwipe == false{
            return
        }
        
        if recognizer.direction == UISwipeGestureRecognizerDirection.left{
            showSideBar(false)
            delegate?.sideBarWillClose?()
        }else if recognizer.direction == UISwipeGestureRecognizerDirection.right{
            showSideBar(true)
            delegate?.sideBarWillOpen?()
        }
    }
    
    func showSideBar(_ shouldOpen: Bool){
        animator.removeAllBehaviors()
        isSideBarOpen = shouldOpen
        
        var gravityX: CGFloat = shouldOpen ? 2.5 : -2.5
        var magnitude: CGFloat = shouldOpen ? 20 : -20
        var boundaryX: CGFloat = shouldOpen ? barWidth : -barWidth - 1
        
        if UIApplication.shared.userInterfaceLayoutDirection == UIUserInterfaceLayoutDirection.rightToLeft || Locale.preferredLanguages[0].hasPrefix("ar"){
            gravityX = shouldOpen ? -2.5 : 2.5
            magnitude = shouldOpen ? -20 : 20
            boundaryX = shouldOpen ? originView.frame.size.width - barWidth : originView.frame.size.width + barWidth + 1
        }
        //print(boundaryX)
        
        let gravityBehavior: UIGravityBehavior = UIGravityBehavior(items: [sideBarContainerView])
        gravityBehavior.gravityDirection = CGVector(dx: gravityX, dy: 0)
        animator.addBehavior(gravityBehavior)
        
        let collisionBehavior: UICollisionBehavior = UICollisionBehavior(items: [sideBarContainerView])
        collisionBehavior.addBoundary(withIdentifier: "sideBarBoundary" as NSCopying, from: CGPoint(x: boundaryX, y: 20), to: CGPoint(x: boundaryX, y: originView.frame.size.height))
        animator.addBehavior(collisionBehavior)
        
        let pushBehavior: UIPushBehavior = UIPushBehavior(items: [sideBarContainerView], mode: UIPushBehaviorMode.instantaneous)
        pushBehavior.magnitude = magnitude
        animator.addBehavior(pushBehavior)
        
        let sideBarBehavior: UIDynamicItemBehavior = UIDynamicItemBehavior(items: [sideBarContainerView])
        sideBarBehavior.elasticity = 0.3
        animator.addBehavior(sideBarBehavior)
        
    }
    
    func sideBarControlDidSelectRow(_ indexPath: IndexPath) {
        if shouldCloseOnSelection{
            showSideBar(!isSideBarOpen)
        }
    }
    
    func removeFromSuperview(){
        sideBarContainerView.removeFromSuperview()
    }
    
    func onLefnavTap(_ gesture: UITapGestureRecognizer){
        if self.isSideBarOpen{
            self.showSideBar(false)
        }else{
            self.showSideBar(true)
        }
    }
    
    func reloadSidebar(){
        
    }
    
    func isIOS8()->Bool{
        var result: Bool = false;
        switch UIDevice.current.systemVersion.compare("8.0.0", options: NSString.CompareOptions.numeric) {
        case .orderedSame, .orderedDescending:
            result = true;
        default:
            result = false;
        }
        return result
    }
   
}
