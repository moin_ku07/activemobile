//
//  AppDelegate.swift
//  FoodMenu
//
//  Created by Moin Uddin on 7/25/16.
//  Copyright © 2016 Moin Uddin. All rights reserved.
//

import UIKit
import GoogleMaps

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    var defaultSession: URLSession = URLSession(configuration: URLSessionConfiguration.default)

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        GMSServices.provideAPIKey("AIzaSyBNyIb1TUcRqs9dZ2yALFCW4Ezgy6V3zeE")
        
        UINavigationBar.appearance().titleTextAttributes = [NSFontAttributeName: UIFont(name: "BahijTheSansArabicExtraBold", size: 20)!, NSForegroundColorAttributeName: UIColor.black]
        
        /*for fontFamily in UIFont.familyNames(){
            print("\(fontFamily)")
            for fontname in UIFont.fontNamesForFamilyName(fontFamily ){
                print("   \(fontname)")
            }
        }*/
        
        let urlPath: String = "http://demos.durlov.com/activemobile.php"
        let url: URL = URL(string: urlPath)!
        
        defaultSession.dataTask(with: url) { (data: Data?, response: URLResponse?, error: Error?) in
            do {
                let jsonResult: NSDictionary = try JSONSerialization.jsonObject(with: data!, options: []) as! NSDictionary
                //print(jsonResult)
                if jsonResult.object(forKey: "enabled") as? Bool == false{
                    DispatchQueue.main.async(execute: {
                        
                        let vc: UIViewController = UIViewController()
                        vc.view.backgroundColor = UIColor.red
                        
                        let label: UILabel = UILabel()
                        label.textColor = UIColor.white
                        label.textAlignment = .center
                        label.font = UIFont.boldSystemFont(ofSize: 20)
                        label.numberOfLines = 0
                        
                        label.text = jsonResult.object(forKey: "message") as? String
                        
                        label.translatesAutoresizingMaskIntoConstraints = false
                        vc.view.addSubview(label)
                        
                        vc.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-[label]-|", options: NSLayoutFormatOptions(), metrics: nil, views: ["label" : label]))
                        vc.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-[label]-|", options: NSLayoutFormatOptions(), metrics: nil, views: ["label" : label]))
                        
                        
                        UIApplication.shared.keyWindow!.rootViewController = vc
                    })
                }
            } catch{
                print(error.localizedDescription)
            }
        }
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

extension UINavigationBar {
    
    func setBottomBorderColor(_ color: UIColor, height: CGFloat) {
        let bottomBorderRect = CGRect(x: 0, y: frame.height, width: frame.width, height: height)
        let bottomBorderView = UIView(frame: bottomBorderRect)
        bottomBorderView.backgroundColor = color
        addSubview(bottomBorderView)
    }
}

// MARK: - CALayer borderColorFromUIColor
extension CALayer{
    func setBorderColorFromUIColor(_ color: UIColor){
        self.borderColor = color.cgColor
    }
}

