//
//  FoodMenuTableViewCell.swift
//  FoodMenu
//
//  Created by Moin Uddin on 7/26/16.
//  Copyright © 2016 Moin Uddin. All rights reserved.
//

import UIKit

class FoodMenuTableViewCell: UITableViewCell {

    @IBOutlet weak var thumb: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var subtitle: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var quantityLabel: UILabel!
    @IBOutlet weak var quantitybg: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        quantitybg.image = UIImage(named: "nocountbg")!
        
        if UIApplication.shared.userInterfaceLayoutDirection == UIUserInterfaceLayoutDirection.rightToLeft{
            var scalingTransform : CGAffineTransform!
            scalingTransform = CGAffineTransform(scaleX: -1, y: 1)
            quantitybg.transform = scalingTransform
        }
        
        if let count: Int = (quantityLabel.text! as NSString).integerValue, count > 0{
            quantitybg.image = UIImage(named: "countbg")!
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func onAddTap(_ sender: UIButton) {
        if let count: Int = (quantityLabel.text! as NSString).integerValue{
            quantityLabel.text = "\(count + 1)"
            quantitybg.image = UIImage(named: "countbg")!
        }
    }

}
