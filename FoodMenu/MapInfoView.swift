//
//  MapInfoView.swift
//  FoodMenu
//
//  Created by Moin Uddin on 7/25/16.
//  Copyright © 2016 Moin Uddin. All rights reserved.
//

import UIKit

class MapInfoView: UIView {

    @IBOutlet weak var imageContainer: UIView!
    @IBOutlet weak var imageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let path = UIBezierPath(roundedRect:imageContainer.bounds, byRoundingCorners:[.topRight, .topLeft], cornerRadii: CGSize(width: (imageContainer.bounds.width)/2, height: (imageContainer.bounds.width-3)/2))
        let maskLayer = CAShapeLayer()
        maskLayer.path = path.cgPath
        //imageView.layer.mask = maskLayer
        //imageContainer.layer.mask = maskLayer
        
        imageContainer.layer.cornerRadius = (imageContainer.bounds.width) / 2
        
    }
    
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

}
