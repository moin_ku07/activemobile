//
//  LoginViewController.swift
//  FoodMenu
//
//  Created by Moin Uddin on 7/25/16.
//  Copyright © 2016 Moin Uddin. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    
    @IBOutlet weak var sliderContainer: UIView!
    @IBOutlet weak var menuListButton: TopIconButton!
    @IBOutlet weak var restaurantFinderButton: TopIconButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.parent?.navigationItem.title = NSLocalizedString("Label_ActiveMobile", comment: "Active Mobile").uppercased()
        
        // Update logincontainer height for iPhone4
        /*if view.frame.size.height == 480{
            let sliderCns: NSLayoutConstraint = NSLayoutConstraint(item: sliderContainer, attribute: NSLayoutAttribute.Height, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute: NSLayoutAttribute.Height, multiplier: 1.0, constant: 190)
            self.view.needsUpdateConstraints()
            for cns in self.view.constraints{
                if cns.identifier == "heightCns"{
                    print("here")
                    self.view.removeConstraint(cns)
                }
            }
            self.view.addConstraint(sliderCns)
            self.view.setNeedsLayout()
        }*/
        
        var iconImage: UIImage = UIImage(named: "icon-menu")!.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        menuListButton.setImage(iconImage, for: UIControlState())
        menuListButton.tintColor = UIColor.white
        
        iconImage = UIImage(named: "icon-finder")!.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        restaurantFinderButton.setImage(iconImage, for: UIControlState())
        restaurantFinderButton.tintColor = UIColor.white
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func onLoginTap(_ sender: UIButton) {
        
        (UIApplication.shared.delegate as! AppDelegate).window?.rootViewController = self.storyboard?.instantiateViewController(withIdentifier: "HomeNavigationViewController") as! HomeNavigationViewController
    }
    
    @IBAction func onMenuButtonTap(_ sender: UIButton) {
        (self.parent as! HomeViewController).setMenuViewAsActive()
    }
    
    
    @IBAction func onFinderButtonTap(_ sender: UIButton) {
        (self.parent as! HomeViewController).setFinderAsActive()
    }
    

}

